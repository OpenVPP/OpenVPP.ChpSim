# ChpSim

A simulation model of a combined heat and power plant.

## Status

* [![pipeline status](https://gitlab.com/openvpp/openvpp.chpsim/badges/master/pipeline.svg)](https://gitlab.com/openvpp/openvpp.chpsim/commits/master)
* [![coverage report](https://gitlab.com/openvpp/openvpp.chpsim/badges/master/coverage.svg)](https://gitlab.com/openvpp/openvpp.chpsim/commits/master)

## Installing Python

Under Ubuntu:

    sudo add-apt-repository ppa:deadsnakes/ppa
    sudo apt install python3.5-dev python3.6-dev

Under Windows:

* Download and run Python 3.5
  * Select **Add to Path**
  * Select **Customize Installation**
  * Select **Install for all users**

* Download and run Python 3.6
  * Select **Add to Path**
  * Select **Customize Installation**
  * Select **Install for all users**

## Ensuring pip

    python -m ensurepip

## Running Tests

    tox

## Activating the Virtual Environment

Under BASH:

    source .tox\py36\bin\activate

Under Windows:

    .tox\py36\Scripts\activate

## Using

    mosaik-chpsim

## Deactivating the Virtual Environment

    deactivate
