from openvpp.chpsim.simulator import ChpSimulator

import mosaik


SIM_CONFIG = {
    'ChpSim': {
        'python': ChpSimulator.__module__ + ':' + ChpSimulator.__name__,
    },
}

START_DATE = '2010-04-08T00:00:00+02:00'  # TODO get from DKDB Client
END = 1 * 6 * 60 * 60  # planning interval of 6 h in seconds

N_CHPS = 15
CHP_TYPE = '50_kw_el'

CHP_TYPES = {
    'default': {
        'chp_p_levels': [(0, 0), (1000, 2000)],
        'chp_min_off_time': 15,
        'storage_e_th_init': .5,
        'storage_e_th_max': 0,
    },
    '5_5_kw_el': {
        'chp_p_levels': [(0, 0), (5500, 12700)],
        'chp_min_off_time': 15,
        'storage_e_th_init': 12700,
        'storage_e_th_max': 25400,
    },
    '4_7_kw_el': {
        'chp_p_levels': [(0, 0), (4700, 12500)],
        'chp_min_off_time': 15,
        'storage_e_th_init': 12500,
        'storage_e_th_max': 25000,
    },
    '50_kw_el': {
        'chp_p_levels': [(0, 0), (49950, 99900)],
        'chp_min_off_time': 30,
        'storage_e_th_init': 47500,
        'storage_e_th_max': 95000,
    },
}


def setup_chps(world):
    """Set-up the ChpSim and return a list of the CHP entities."""
    chpsim = world.start('ChpSim', start_date=START_DATE)
    chps = chpsim.CHP.create(N_CHPS, **CHP_TYPES[CHP_TYPE])
    return chps


def create_scenario(world):
    chps = setup_chps(world)
    assert len(chps) == N_CHPS


def main():
    world = mosaik.World(SIM_CONFIG)
    create_scenario(world)
    world.run(until=END)


def test_chp_simulator():
    main()
